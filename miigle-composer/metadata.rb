name             'miigle-composer'
maintainer       'Josh Fester'
maintainer_email 'josh.fester@miigle.com'
license          'All rights reserved'
description      'Installs/Configures composer'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'composer'
