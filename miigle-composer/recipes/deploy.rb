include_recipe 'composer'

node[:deploy].each do |application, deploy|

  %w(mod/miigle_dependencies/lib mod/miigle_ideas/lib).each do |path|
    composer_project "#{deploy[:absolute_document_root]}#{path}" do
      action :install
    end
  end

end
