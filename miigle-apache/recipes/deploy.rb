include_recipe 'deploy::default'
include_recipe 'apache2::service'

node[:deploy].each do |application, deploy|

  opsworks_deploy_dir do
    user deploy[:user]
    group deploy[:group]
    path deploy[:deploy_to]
  end

  opsworks_deploy do
    deploy_data deploy
    app application
  end

  if platform?('ubuntu') && node[:platform_version] == '14.04'
    web_app deploy[:application] do
      docroot deploy[:absolute_document_root]
      server_name deploy[:domains].first
      unless deploy[:domains][1, deploy[:domains].size].empty?
        server_aliases deploy[:domains][1, deploy[:domains].size]
      end
      mounted_at deploy[:mounted_at]
      ssl_certificate_ca deploy[:ssl_certificate_ca]
    end
  end

  # move away default virtual host so that the new app becomes the default virtual host
  if platform?('ubuntu') && node[:platform_version] == '14.04'
    source_default_site_config = "#{node[:apache][:dir]}/sites-enabled/000-default.conf"
    target_default_site_config = "#{node[:apache][:dir]}/sites-enabled/zzz-default.conf"
  else
    source_default_site_config = "#{node[:apache][:dir]}/sites-enabled/000-default"
    target_default_site_config = "#{node[:apache][:dir]}/sites-enabled/zzz-default"
  end
  execute 'mv away default virtual host' do
    action :run
    command "mv #{source_default_site_config} #{target_default_site_config}"
    notifies :reload, "service[apache2]", :delayed
    only_if do
      ::File.exists?(source_default_site_config)
    end
  end
end
