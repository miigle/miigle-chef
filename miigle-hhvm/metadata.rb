name             'miigle-hhvm'
maintainer       'Josh Fester'
maintainer_email 'josh.fester@miigle.com'
license          'All rights reserved'
description      'Installs/Configures hhvm'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'hhvm'
