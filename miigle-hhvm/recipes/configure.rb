service "hhvm" do
  action :nothing
  supports :status => true, :start => true, :stop => true, :restart => true
end

template "/etc/hhvm/server.ini" do
    source "server.ini.erb"
    mode 0660
    owner "root"
    notifies :restart, "service[hhvm]", :delayed
end

template "/etc/hhvm/php.ini" do
    source "php.ini.erb"
    mode 0660
    owner "root"
    notifies :restart, "service[hhvm]", :delayed
end
