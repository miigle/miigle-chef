
node[:deploy].each do |application, deploy|

  template "#{deploy[:absolute_document_root]}/engine/settings.php" do
    source "settings.php.erb"
    mode 0660
    owner "www-data"
    group "www-data"
    variables(
      :database   => (deploy[:database][:database] rescue nil),
      :user       => (deploy[:database][:username] rescue nil),
      :password   => (deploy[:database][:password] rescue nil),
      :host       => (deploy[:database][:host] rescue nil),
      :keys       => (keys rescue nil)
    )
  end

  template "#{deploy[:absolute_document_root]}/.htaccess" do
    source "htaccess.erb"
    mode 0660
    owner "www-data"
    group "www-data"
  end

end
