name             'miigle-elgg'
maintainer       'Josh Fester'
maintainer_email 'josh.fester@miigle.com'
license          'All rights reserved'
description      'configures elgg'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
